Source: tilix
Section: gnome
Priority: optional
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: @GNOME_TEAM@
Build-Depends: debhelper-compat (= 12),
               dh-dlang,
               gnome-pkg-tools (>= 0.19.8),
               libgtkd-3-dev (>= 3.9),
               librsvg2-dev,
               libunwind-dev,
               libvted-3-dev (>= 3.9),
               meson,
               po4a
Standards-Version: 4.4.1
Vcs-Browser: https://salsa.debian.org/gnome-team/tilix
Vcs-Git: https://salsa.debian.org/gnome-team/tilix.git
Homepage: https://gnunn1.github.io/tilix-web/

Package: tilix
Architecture: any
Depends: tilix-common (= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Provides: x-terminal-emulator
Replaces: terminix (<< 1.5.4-1~)
Breaks: terminix (<< 1.5.4-1~)
Suggests: python-nautilus
Description: Tiling terminal emulator for GNOME
 Tilix is a feature-rich tiling terminal emulator following the
 GNOME human interface design guidelines.
 Its many features include:
 .
  * Layout terminals in any fashion by splitting them horizontally or
    vertically.
  * Terminals can be re-arranged using drag and drop both within and
    between windows.
  * Terminals can be detached into a new window via drag and drop.
  * Input can be synchronized between terminals so commands typed in
    one terminal are replicated to the others.
  * Supports notifications when processes are completed out of view.

Package: tilix-common
Architecture: all
Depends: ${misc:Depends},
         ${shlibs:Depends}
Replaces: terminix-common (<< 1.5.4-1~)
Breaks: terminix-common (<< 1.5.4-1~)
Description: Tiling terminal emulator - data files
 Tilix is a feature-rich tiling terminal emulator following the
 GNOME human interface design guidelines.
 .
 This package contains architecture independent data.
